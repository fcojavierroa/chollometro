package com.bestsecret.chollometro;

import com.bestsecret.chollometro.CholloConfiguration.VariantPrice;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.KafkaTemplate;

import javax.annotation.PreDestroy;
import java.util.Date;

@SpringBootApplication
@EnableKafkaStreams
public class ChollometroApplication implements ApplicationRunner {

	@Autowired
	StreamsBuilder streamsBuilder;

	@Autowired
	KafkaStreamsConfiguration kafkaStreamsConfiguration;

	@Autowired
	KafkaTemplate kafkaTemplate;

	@Value("${chollo.topic}")
	String topic;

	private KafkaStreams kafkaStreams;

	public static void main(String[] args) {
		SpringApplication.run(ChollometroApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		populateTopic();
		cleanAndStartStreams();
	}

	private void populateTopic() {
		VariantPrice price1 = new VariantPrice();
		price1.baseProductId = "no-chollo";
		price1.variantCode = "003";
		price1.recommendedRetailPrice = 200.0f;
		price1.retailPrice = 190f;
		price1.messageId=Long.toString(new Date().getTime());
		price1.colorId="123";
		price1.eventDate=new Date();
		price1.size="XL";
		price1.sizeRun="112";

		kafkaTemplate.send(topic, price1.messageId, price1);
	}

	private void cleanAndStartStreams() {
		kafkaStreams = new KafkaStreams(streamsBuilder.build(),
				kafkaStreamsConfiguration.asProperties());
		kafkaStreams.cleanUp();
		kafkaStreams.start();
	}

	@PreDestroy
	public void onExit() {
		kafkaStreams.close();
	}
}
