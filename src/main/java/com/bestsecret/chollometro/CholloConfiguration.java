package com.bestsecret.chollometro;

import lombok.Data;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.Arrays;
import java.util.Date;

@Configuration
public class CholloConfiguration {

    @Value("${chollo.topic}")
    String topic;

    @Value("${chollo.threshold}")
    Float threshold;

    @Bean
    public KStream<String, VariantPrice> readChollos(StreamsBuilder builder) {
        KStream<String, VariantPrice> stream = builder.stream(topic,
                Consumed.with(Serdes.String(), new JsonSerde<>(VariantPrice.class)));
        stream
            .filter((k, v) -> 1 - v.retailPrice / v.recommendedRetailPrice >= threshold)
            .peek((k, v) -> System.out.println("CHOLLO ENCONTRADO : "
                    + (1 - v.retailPrice / v.recommendedRetailPrice) + " de descuento - " + v));

        return stream;
    }

    //@Bean
    public KStream<String, VariantPrice> getTopChollos(StreamsBuilder builder) {
        KStream<String, VariantPrice> stream = builder.stream(topic,
                Consumed.with(Serdes.String(), new JsonSerde<>(VariantPrice.class)));

        stream.selectKey((k, v) -> v.baseProductId + "-" + v.variantCode)
                .groupByKey()
                .aggregate(ChollosTop3::new, (k, v, top3) -> {
                    top3.nrs[3] = v;
                    Arrays.sort(top3.nrs, (a,b) -> {
                        if (a==null) return 1;
                        if (b==null) return -1;
                        return Float.compare((1 - b.retailPrice /b.recommendedRetailPrice),  (1 - a.retailPrice / a.recommendedRetailPrice));
                    });
                    top3.nrs[3] =null;
                    return top3;
                });

        return stream;
    }

    @Data
    public static class VariantPrice {
        String messageId;
        Date eventDate;
        String baseProductId;
        String colorId;
        Float recommendedRetailPrice;
        Float retailPrice;
        String size;
        String sizeRun;
        String variantCode;
    }

    static class ChollosTop3 {
        public VariantPrice [] nrs = new VariantPrice[4];
    }


}
